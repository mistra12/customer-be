package com.minayasa.customer.controller;

import com.minayasa.customer.model.request.CustomerRequest;
import com.minayasa.customer.model.response.CustomerResponse;
import com.minayasa.customer.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    public ResponseEntity<CustomerResponse> addCustomer(@RequestBody CustomerRequest request) {
        CustomerResponse response = customerService.addCustomer(request);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<CustomerResponse>> getCustomer( @RequestParam(value = "nama",  required = false) String nama,
                                                               @RequestParam(value = "alamat", required = false) String alamat,
                                                               @RequestParam(value = "kota", required = false) String kota,
                                                               @RequestParam(value = "provinsi", required = false) String provinsi,
                                                               @RequestParam(value = "status", required = false) String status) {
        return ResponseEntity.ok(customerService.getCustomer(nama, alamat, kota, provinsi, status));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerResponse> getCustomerById(@PathVariable Long id) {
        return ResponseEntity.ok(customerService.getCustomer(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerResponse> updateCustomer(@PathVariable Long id, @RequestBody CustomerRequest customerRequest) {
        return ResponseEntity.ok(customerService.updateCustomer(id, customerRequest));
    }
}
