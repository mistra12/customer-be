package com.minayasa.customer.service.impl;

import com.minayasa.customer.entity.Customer;
import com.minayasa.customer.model.request.CustomerRequest;
import com.minayasa.customer.model.response.CustomerResponse;
import com.minayasa.customer.repository.CustomerRepository;
import com.minayasa.customer.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;
    @Override
    @Transactional
    public CustomerResponse addCustomer(CustomerRequest request) {
        Customer customer = new Customer();
        BeanUtils.copyProperties(request, customer);
        Customer savedCustomer = customerRepository.save(customer);
        return CustomerResponse.builder()
                .id(savedCustomer.getId())
                .nama(savedCustomer.getNama())
                .alamat(savedCustomer.getAlamat())
                .kota(savedCustomer.getKota())
                .provinsi(savedCustomer.getProvinsi())
                .tglRegistrasi(savedCustomer.getTglRegistrasi())
                .status(savedCustomer.getStatus())
                .build();
    }

    @Override
    public CustomerResponse getCustomer(Long id) {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new RuntimeException("Data Not Found"));
        return CustomerResponse.builder()
                .id(customer.getId())
                .nama(customer.getNama())
                .alamat(customer.getAlamat())
                .kota(customer.getKota())
                .provinsi(customer.getProvinsi())
                .tglRegistrasi(customer.getTglRegistrasi())
                .status(customer.getStatus())
                .build();
    }

    @Override
    public List<CustomerResponse> getCustomer(String nama, String alamat, String kota, String provinsi, String status) {
        List<Customer> customers = customerRepository.findAll();
        if (nama != null || alamat != null || kota != null || provinsi != null || status != null) {
            if (nama == null) {
                nama = "";
            }
            if(alamat == null) {
                alamat = "";
            }
            if (kota == null) {
                kota = "";
            }

            if (provinsi == null) {
                provinsi = "";
            }

            customers = customerRepository.getCustomerFilter(nama, alamat, kota, provinsi, status);
        }
        return customers.stream().map(customer -> CustomerResponse.builder()
                .id(customer.getId())
                .nama(customer.getNama())
                .alamat(customer.getAlamat())
                .kota(customer.getKota())
                .provinsi(customer.getProvinsi())
                .tglRegistrasi(customer.getTglRegistrasi())
                .status(customer.getStatus())
                .build()).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public CustomerResponse updateCustomer(Long id, CustomerRequest customerRequest) {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new RuntimeException("Data not found"));
        customer.setNama(customerRequest.getNama());
        customer.setAlamat(customerRequest.getAlamat());
        customer.setKota(customerRequest.getKota());
        customer.setProvinsi(customerRequest.getProvinsi());
        customer.setTglRegistrasi(customerRequest.getTglRegistrasi());
        customer.setStatus(customerRequest.getStatus());

        Customer updateCustomer = customerRepository.save(customer);

        return CustomerResponse.builder()
                .id(updateCustomer.getId())
                .nama(updateCustomer.getNama())
                .alamat(updateCustomer.getAlamat())
                .kota(updateCustomer.getKota())
                .provinsi(updateCustomer.getProvinsi())
                .tglRegistrasi(updateCustomer.getTglRegistrasi())
                .status(updateCustomer.getStatus())
                .build();
    }

}
