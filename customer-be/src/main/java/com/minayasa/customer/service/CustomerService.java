package com.minayasa.customer.service;

import com.minayasa.customer.model.request.CustomerRequest;
import com.minayasa.customer.model.response.CustomerResponse;

import java.util.List;

public interface CustomerService {
    CustomerResponse addCustomer(CustomerRequest request);
    CustomerResponse getCustomer(Long id);
    List<CustomerResponse> getCustomer(String nama, String alamat, String kota, String provinsi, String status);
    CustomerResponse updateCustomer(Long id, CustomerRequest customerRequest);
}
