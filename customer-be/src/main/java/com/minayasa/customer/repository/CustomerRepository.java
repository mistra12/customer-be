package com.minayasa.customer.repository;

import com.minayasa.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    String queryFilter = "select c.* from customers c " +
            "where (:nama = '' OR (c.nama ILIKE %:nama%)) AND " +
            "(:alamat = '' OR (c.alamat ILIKE %:alamat%)) AND " +
            "(:kota = '' OR (c.kota ILIKE %:kota%)) AND " +
            "(:provinsi = '' OR (c.provinsi ILIKE %:provinsi%)) AND " +
            "(:status = '' OR (c.status = status)) ";

    @Query(value = queryFilter, nativeQuery = true)
    List<Customer> getCustomerFilter(@Param("nama") String nim,
                                     @Param("alamat") String username,
                                     @Param("kota") String email,
                                     @Param("provinsi") String provinsi,
                                     @Param("status") String status);
}
