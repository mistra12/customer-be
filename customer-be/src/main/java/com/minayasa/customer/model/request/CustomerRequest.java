package com.minayasa.customer.model.request;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class CustomerRequest {
    private String nama;
    private String alamat;
    private String kota;
    private String provinsi;
    private LocalDateTime tglRegistrasi;
    private String status;
}
