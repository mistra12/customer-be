package com.minayasa.customer.model.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class CustomerResponse {
    private Long id;
    private String nama;
    private String alamat;
    private String kota;
    private String provinsi;
    private LocalDateTime tglRegistrasi;
    private String status;
}
